var app         = require('connect')()
var serveStatic = require('serve-static')
var watch       = require('node-watch')

app.use(serveStatic('public'))


watch('c://xampp/htdocs/frontend-test/', { recursive: true }, function(evt, name) {
    console.log('%s changed.', name);
}); 


console.log(' ➜   Open: http://localhost:7007')
watch('/', {recursive: true}, app.listen(7007), console.log('server waiting...') )