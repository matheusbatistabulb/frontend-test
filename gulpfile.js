'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
  return gulp.src('./public/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('templates', function() {
  return gulp.src('./public/javascripts/*.js')
  .pipe(handlebars())
  .pipe(wrap('Handlebars.template(<%= contents %>)'))
  .pipe(declare({
    namespace: 'ranking.bundled',
    noRedeclare: true,
  }))
  .pipe(concat('templates.js'))
  .pipe(gulp.dest('./public/javascripts'))
})
 
gulp.task('watch', function () {
  gulp.watch('./public/sass/**/*.scss', gulp.series('sass'));
});