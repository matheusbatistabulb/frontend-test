// your code

var celebritiesTemplate = require("./celebritiesTemplate.hbs");

// fetch("../fazenda.json")
// .then(response => {
//     return response.json();
// })
// .then(data => {

//     if (data) {
//     createHtml(data);
//     }
//     // console.log(data);

//     Handlebars.registerHelper("probabilities", function(positive, negative) {
//     var hundred = positive + negative;
//     var posPercent = Math((positive * 100) / hundred);
//     var negPercent = Math((negative * 100) / hundred);
//     });

//     function createHtml(celebritiesData) {
//     console.log("retrieving data");
//     console.log(celebritiesData);

//     var rankingContainer = document.getElementById("ranking");
//     rankingContainer.innerHTML = celebritiesTemplate(celebritiesData);

//     // var htmlTemplate = document.getElementById("celebrityStats").innerHTML;
//     // var compiledTemplate = Handlebars.compile(htmlTemplate);
//     // var genHTML = compiledTemplate(celebritiesData);

//     // var rankingBox = document.getElementById("ranking");
//     // rankingBox.innerHTML = genHTML;
//     }
// })
// .catch(err => {
//     console.log(err);
// });

var ourRequest = new XMLHttpRequest();
ourRequest.open('GET', '../fazenda.json');
ourRequest.onload = function() {
  if (ourRequest.status >= 200 && ourRequest.status < 400) {
    var data = JSON.parse(ourRequest.responseText);
    createHTML(data);
  } else {
    console.log("We connected to the server, but it returned an error.");
  }
};

ourRequest.onerror = function() {
  console.log("Connection error");
};

ourRequest.send();

function createHTML(petsData) {
  var rankingContainer = document.getElementById("ranking");
  rankingContainer.innerHTML = celebritiesTemplate(petsData);
}