      var path = require("path");

      module.exports = {
        mode: "development",
        entry: {
          js: "./public/javascripts/main.js",
          css: "./public/sass/style.scss"
        },
        output: {
          path: path.resolve(__dirname, "./public"),
          filename: '[name].js',
          publicPath: "./public"
        },
        resolve: {
          alias: {
            javascripts: path.join(__dirname, "./public")
          }
        },
        module: {
          rules: [
            { test: /\.hbs$/, loader: "@icetee/handlebars-loader" },
            { test: /\.(s*)css$/,
              use: ['style-loader', 'css-loader', 'sass-loader']}
          ]
        }
      };


